#软件名称：MDEditor#
##软件用途：markdown格式文档编辑和预览。##
>MEditor is text editor with multiple windows. You can use it as markdown editor and preview. 

- 可应用于写博客，可以写好后将html剪切发布即可；
- 支持标准Markdown语法，支持[GitHub Flavored Markdown](http://github.github.com/github-flavored-markdown/)
- 即时预览
- 团队协作之间的文档编辑与阅读，纯文本格式，便于版本比较；html预览，便于阅读

###各种功能还在开发完善中。。。。。。

###发起人：一挥，[新浪微博](http://weibo.com/5d13 "一挥间的新浪微博"),[博客](http://cnblogs.com/yihuiso "一挥间的博客")
###现有开发者：dodola，[github](https://github.com/dodola) [腾讯微博](http://t.qq.com/dododream)
###许可协议：Apache License, Version 2.0 ###
